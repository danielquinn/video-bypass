# video-bypass

Watch YouTube videos locally by clicking a button.

## The State of This Project

It's rough.  I hacked this together in one evening after being frustrated with YouTube's shit UI.  I lack any semblance of design sense, so everything about the visual presentation is abysmal (merge requests welcome!)

It does work however, surprisingly well... _on my machine anyway_.  I'm running Firefox in GNOME on Arch Linux, so if you're on Windows... well you're on your own.


## Installation

The process isn't nearly as automated as I want to make it, but for now it's a few steps:


### 1. Download the bypass program

It's in this repo, so you can just download it straight from GitLab if you
want.  Just put it somewhere in your `${PATH}` like `/usr/local/bin/`.


### 2. Create the `.desktop` file

You need a file in a special place in your home folder to tell your computer
about the `bypass` script we just downloaded.

Create a file called `${HOME}/.local/share/applications/video-bypass.desktop`
and put this in it:

```
[Desktop Entry]
Name=Video Bypass
Exec=bypass %u
Icon=/path/to/this/repo/icons/border-48.png
Type=Application
Terminal=false
MimeType=x-scheme-handler/video-bypass;
```


### 3. Tell your machine when to run the `bypass` program

This whole thing works by telling your computer to run our `bypass` program
whenever it sees a URL in the format `video-bypass://...`.  This is managed
with a couple XDG utilities that _should_ exist so long as you're running
Linux.

```shell
$ xdg-mime default emacsclient.desktop x-scheme-handler/org-protocol
```

### 4. Update your desktop database

That `.desktop` may not do anything unless you tell your computer about it,
so for that, we need this:

```shell
$ update-desktop-database ~/.local/share/applications
```

### That sounds complicated

It's certainly not user-friendly.  I'd like to see if there's a way to
automate the process, but until then, you can just do all this with a
one-liner if that makes you happy:

```shell
cd ${HOME}/.local/share/applications && echo "[Desktop Entry]
Name=Video Bypass
Exec=/usr/local/bin/bypass %u
Icon=/path/to/this/repo/icons/border-48.png
Type=Application
Terminal=false
MimeType=x-scheme-handler/video-bypass;" > video-bypass.desktop && xdg-mime default video-bypass.desktop x-schema-handler/video-desktop && update-desktop-database ${HOME}/.local/share/applications
```

### 5. Install this extension

It's not on addons.mozilla.org yet, so until then, you have to do it manually
by following [Mozilla's instructions](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing).