package main

import (
	"log"
	"os"
	"os/exec"
	"strings"
)

func main() {

	videoId := strings.Split(os.Args[1], "//")[1]
	yt, err := exec.LookPath("yt-dlp")
	if err != nil {
		log.Fatal(err)
	}

	mpv, err := exec.LookPath("mpv")
	if err != nil {
		log.Fatal(err)
	}

	c1 := exec.Command(yt, "https://youtube.com/watch?v="+videoId, "-o", "-")
	c2 := exec.Command(mpv, "-")
	c2.Stdin, _ = c1.StdoutPipe()
	c2.Stdout = os.Stdout

	_ = c2.Start()
	_ = c1.Run()
	_ = c2.Wait()

}
