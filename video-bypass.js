const this_url = new URL(document.location);
const video_id = this_url.searchParams.get('v');

if (video_id) {

    const a = document.createElement('a');

    a.setAttribute('style', 'padding: 1rem 3rem; margin: 1rem; background-color: #333; color: #ffffff; position: absolute; top: 1rem; left: 1rem; z-index: 9999; display: block; border: 1px solid #ffffff; border-radius: 0.5rem;');
    a.setAttribute('href', 'video-bypass://' + video_id);
    a.setAttribute('id', 'video-bypass-button');
    a.innerHTML += 'Bypass';

    document.body.appendChild(a);

}
